# Why not have a advanced deploy configuration with Helm harts, auto scaling, recovery, monitoring, etc?

In that repo we have only a simple working example of a deployment but we can have full magic in a real project ...

I'm deploying the backend and ui in same chart but that is not common for big projects. If the chart is tooo big then it will have a huge delay ...
Smaller charts can interoperate with each other and can be deployed in parallel.

## How to deploy?

First setup a k8s cluster and your access then:

```bash
helm install cct --create-namespace -n claimclamtest .
```

```bash
helm upgrade cct -n claimclamtest .
```

For production like environments I use a script to get the latest version using semver and deploying it (I have a magic golang program for that)
