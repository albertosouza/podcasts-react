{{/*
I use job for node used on processing:
*/}}
{{- define "spec.preemptive" -}}
tolerations:
  - effect: NoSchedule
    key: role
    operator: Equal
    value: job
nodeSelector:
  role: job
{{- end }}
