package http_client

import "net/http"

// mockable http client:
type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type HttpClientST struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

func (m *HttpClientST) Do(req *http.Request) (*http.Response, error) {
	if m.DoFunc != nil {
		return m.DoFunc(req)
	}
	// just in case you want default correct return value
	return &http.Response{}, nil
}

func NewClient() HttpClient {
	return &http.Client{}
}
