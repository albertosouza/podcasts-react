package cmd

import (
	"log"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/spf13/cobra"
	"gitlab.com/albertosouza/cctest1/graph"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

const defaultPort = "8080"

var StartDevServerCMD = &cobra.Command{
	Use:   "start-dev-server",
	Short: "Start http server",
	Run: func(cmd *cobra.Command, args []string) {
		port := os.Getenv("PORT")
		if port == "" {
			port = defaultPort
		}

		e := echo.New()
		e.Use(middleware.CORS())

		graphqlHandler := handler.NewDefaultServer(graph.NewExecutableSchema(graph.Config{Resolvers: &graph.Resolver{}}))
		playgroundHandler := playground.Handler("GraphQL playground", "/query")

		e.POST("/query", func(c echo.Context) error {
			graphqlHandler.ServeHTTP(c.Response(), c.Request())
			return nil
		})

		e.GET("/", func(c echo.Context) error {
			playgroundHandler.ServeHTTP(c.Response(), c.Request())
			return nil
		})

		log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)

		err := e.Start(":" + port)
		if err != nil {
			log.Fatalln(err)
		}
	},
}
