package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "main",
	Short: "Main ClaimClam test app",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Use 'main -h' or 'go run main.go -h' to view all command options")
	},
}

func init() {
	rootCmd.AddCommand(StartServerCMD)
	rootCmd.AddCommand(StartDevServerCMD)
}

// Execute - Execute the command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Println("cmd error on run command", err)
		os.Exit(1)
	}
}
