package podcast

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/albertosouza/cctest1/graph/model"
	"gitlab.com/albertosouza/cctest1/http_client"
)

var (
	c PodcastClient // mockable client
)

type PodcastClient interface {
	SetURL(string)
	GetURL() string

	QueryPodcast(*model.QueryPoscastsInput) ([]*model.Podcast, error)
}

type Client struct {
	URL        string
	HTTPClient http_client.HttpClient
}

func (c *Client) SetURL(url string) {
	c.URL = url
}

func (c *Client) GetURL() string {
	return c.URL
}

var (
	defaultPage  = 1
	defaultLimit = 50
)

func (c *Client) QueryPodcast(filter *model.QueryPoscastsInput) ([]*model.Podcast, error) {
	req, _ := http.NewRequest("GET", c.URL+"/podcasts", nil)
	req.Header.Add("Accept", "application/json")
	// query filters:
	q := req.URL.Query()

	if filter != nil {
		if filter.Search != nil && *filter.Search != "" {
			q.Add("search", *filter.Search)
		}

		if filter.Title != nil && *filter.Title != "" {
			q.Add("title", *filter.Title)
		}

		if filter.Category != nil && *filter.Category != "" {
			q.Add("category", *filter.Category)
		}

		if filter.Page != nil && *filter.Page > 0 {
			q.Add("page", fmt.Sprintf("%d", *filter.Page))
		} else {
			filter.Page = &defaultPage
		}

		if filter.Limit != nil && *filter.Limit > 0 { // ignoring 0 limit because does not make sense to use it ... i think ...
			q.Add("limit", fmt.Sprintf("%d", *filter.Limit))
		} else {
			filter.Limit = &defaultLimit
		}

		req.URL.RawQuery = q.Encode()
	}

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error querying podcast: %w", err)
	}

	d := []*model.Podcast{}

	switch resp.StatusCode {
	case http.StatusOK:
		err = json.NewDecoder(resp.Body).Decode(&d)
		if err != nil {
			return nil, fmt.Errorf("error decoding podcast: %w", err)
		}

		return d, nil
	case http.StatusNotFound:
		return d, nil
	default:
		return nil, fmt.Errorf("unknow error querying podcast: %w", err)
	}
}

func GetClient() PodcastClient {
	if c == nil {
		c = NewClient()
	}
	return c
}

func NewClient() *Client {
	httpClient := http_client.NewClient()

	return &Client{
		URL:        "https://601f1754b5a0e9001706a292.mockapi.io",
		HTTPClient: httpClient,
	}
}
