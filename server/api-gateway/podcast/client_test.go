package podcast_test

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	approvals "github.com/approvals/go-approval-tests"
	"gitlab.com/albertosouza/cctest1/graph/model"
	"gitlab.com/albertosouza/cctest1/http_client"
	"gitlab.com/albertosouza/cctest1/podcast"
)

func TestClient_QueryPodcast(t *testing.T) {

	type args struct {
		filter *model.QueryPoscastsInput
	}
	tests := []struct {
		name    string
		args    args
		apiBody string
		wantErr bool
	}{
		{
			name: "should get podcasts",
			apiBody: `[
				{
					"id": "40325177-02e5-4ecf-aec5-fc7345d3b83c",
					"title": "The Secrets Hotline",
					"images": {
						"default": "https://img4.luminarypodcasts.com/v1/podcast/40325177-02e5-4ecf-aec5-fc7345d3b83c/default/h_500,w_500/image/s--YOMzvZ-B--/aHR0cHM6Ly9pbWFnZXMubWVnYXBob25lLmZtL0d3bnMwYW1RbzFpNmhlMTFDbkVqUnZUbTUzTm9ndlBjWmhzVTZ4czdLbmcvcGxhaW4vczM6Ly9tZWdhcGhvbmUtcHJvZC9wb2RjYXN0cy9jOTUyNjRjOC02MjRmLTExZWEtOGE2ZC1iMzk1ZTU2YTI0YWEvaW1hZ2UvdXBsb2Fkc18yRjE1ODY1NTA1Mjc3NzQtaWNic3pzY2pyeC1hZWQ4NjgxMWI5MTMyZmY0NTdiYjcyOTM0NDRkZjM2ZF8yRlNlY3JldHNfSG90bGluZV9UaHVtYm5haWwuanBn.jpg",
						"featured": "https://img4.luminarypodcasts.com/v1/podcast/40325177-02e5-4ecf-aec5-fc7345d3b83c/featured/h_430,w_720/image/s--TEXiyJIj--/aHR0cHM6Ly9pbWFnZXMubHVtaW5hcnkuYXVkaW8vaW1hZ2VzLXVzLWVhc3QtMi5wcm9kLmx1bWluYXJ5LmF1ZGlvLzE1ODc3NjE4MTcxMDQ4ODgzODEtU2VjcmV0c19Ib3RsaW5lX0ZlYXR1cmVkLnBuZw==.png",
						"thumbnail": "https://img4.luminarypodcasts.com/v1/podcast/40325177-02e5-4ecf-aec5-fc7345d3b83c/thumbnail/h_360,w_360/image/s--yD3hPQA9--/aHR0cHM6Ly9pbWFnZXMubHVtaW5hcnkuYXVkaW8vaW1hZ2VzLXVzLWVhc3QtMi5wcm9kLmx1bWluYXJ5LmF1ZGlvLzE1ODc3NjE3OTgwMzk2MTE4NDYtU2VjcmV0c19Ib3RsaW5lX1RodW1ibmFpbC5wbmc=.png",
						"wide": "https://img4.luminarypodcasts.com/v1/podcast/40325177-02e5-4ecf-aec5-fc7345d3b83c/wide/h_140,w_225/image/s--kmKs_M15--/aHR0cHM6Ly9pbWFnZXMubHVtaW5hcnkuYXVkaW8vaW1hZ2VzLXVzLWVhc3QtMi5wcm9kLmx1bWluYXJ5LmF1ZGlvLzE1ODc3NjE4MDc3NjQ3MTAzMjAtU2VjcmV0c19Ib3RsaW5lX1dpZGUucG5n.png"
					},
					"isExclusive": true,
					"publisherName": "Love and Radio",
					"publisherId": "7c8226ef-de61-439a-9c51-9865d30ac2d9",
					"mediaType": "podcast",
					"description": "What’s your secret? Listen as anonymous callers from across the globe share what is hidden in their lives. It’s both a voyeuristic and affirming experience. \nLeave your own at 1-929-SECRETS (929-732-7387) or secretshotline.org. New episodes coming this November.",
					"categoryId": "4fb6e6ed-d6ef-4d04-878b-40942df8d241",
					"categoryName": "Performing Arts",
					"hasFreeEpisodes": true,
					"playSequence": "episodic"
				}]`,
			// for failure tests you can add more params here and improve the test execution method bellow:
		},
		{
			name:    "should return a empty list",
			apiBody: `[]`,
			args: args{
				filter: &model.QueryPoscastsInput{
					Search: refString("nothingfound!"),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := podcast.Client{ // test http client
				HTTPClient: &http_client.HttpClientST{
					DoFunc: func(req *http.Request) (*http.Response, error) {
						body := io.NopCloser(bytes.NewReader([]byte(tt.apiBody)))
						return &http.Response{
							StatusCode: 200,
							Body:       body,
						}, nil
					},
				},
			}

			got, err := c.QueryPodcast(tt.args.filter)
			if (err != nil) != tt.wantErr {
				t.Errorf("Client.QueryPodcast() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// playing with approvals ... its not need but sometimes makes checking test result faster and funny to work with TTD:
			approvals.VerifyJSONStruct(t, got)
		})
	}
}

func refString(s string) *string {
	return &s
}
