See project root folder README.md


## Notes:

How to build a new image and push to repo:

```
docker build --tag ccp-api-gateway:release --target release .
docker tag ccp-api-gateway:release $REPO_IMAGE:$IMAGE_NAME
docker push $REPO_IMAGE:$IMAGE_NAME

```

Test the container locally:

```
docker run -it ccp-api-gateway:release ./main -h
docker run -it ccp-api-gateway:release ./main start-server
```

