# Claimclam test result

Related test: https://github.com/GetClaimClam/claimclam-takehome-assessment

For that test I'm versioning all subservices in the same repo to make it easier to run and test. Dont need to clone multiple repos and setup multiple services.
I'm using docker composer to run it localy, seems easier but is possible to run each service individually.

> I used that test to study Next.js, I know many others front end frameworks but working with Next.js was more challenging and I think some logic and way to use it may be not soo good. but works and have most of the tasks done.

> At backend I have more controll then was fast and I added 1 test as a exemple how we could do that.

## About the challenge tasks:

### Requirements
Backend(golang prefeerred):
- [x] Create an api-gateway service for podcasts microservice with basic error handling and input validation functions
UI(next.js preferred):
- [x] Create a controlled input field to fetch podcasts. By default you should request all podcasts on initial load.

- [x] Render a list of podcasts from the request. Feel free to use any data from the response that you find relevant or necessary to show (Don't worry about styling here yet).

- [x] Handle UI states for:
  - [ ]- When the podcasts are being fetched.
      > I implemented something here but did not worked as expected...
  - [x] When the response returns some podasts.
  - [x] When the response returns no podcasts that match the search value.

### Bonus

- [x] Add some styling to the search results. Feel free to render as a list, cards, etc. Whatever would be a nice UI for the end user.
- [x] Add some pagination. The api supports "page" and "limit" params.
    > Done only in the backend
- [x] Add a debounce of 500ms to avoid fetching on every key press.
- [x] Use graphQL as a protocol between UI and API gateway
    > In the Next.js part that was a pain, docs and related modules are not clear and I had to spend a lot of time to make it work.
- [ ] Add basic rate limiting and heartbeat features to API gateway
    >  I dont implemented that but in my last job I implemented a advanced rate limiting feature for authentication and I can explain some extrategies including using echo middlewares.

### Secret stage:

- [x] Added configs for help chart to deloy that projects and I deployed that in my k8s cluster.:
  - https://cct.albertosouza.net/
  - https://cct-ui.albertosouza.net/

## How install

- Download and install docker and docker-compose
- Clone this repo
- Run `docker-compose up -d`

After that the api-gateway and FE part should be up on: http://localhost:5244/query
And the playground at: http://localhost:5244

## How to run

## Services available:

- gql GrappQL playground: http://localhost:5244
- api-gateway: http://localhost:5244/query

## Useful commands:

- Re generate graphql code: go run github.com/99designs/gqlgen generate

## Notes

API docs:

```
Service URL: https://601f1754b5a0e9001706a292.mockapi.io
API Spec
Add query params to GET request:
/podcasts - get all podcasts (https://601f1754b5a0e9001706a292.mockapi.io/podcasts)
/podcasts?search=Comedy - search by all fields for string Comedy
/podcasts?title=The%20Trevor%20Noah%20Podcast - search any field by property name (title)
/podcasts?categoryName=History - search any field by property name (categoryName)

PAGINATION
Add query params to GET requests:

/podcats?page=1&limit=10
or /podcasts?p=1&l=10
```

I used qglgen to setup graphql on BE