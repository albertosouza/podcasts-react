"use client";

import { ApolloLink, HttpLink } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import {
  ApolloNextAppProvider,
  NextSSRApolloClient,
  NextSSRInMemoryCache,
  SSRMultipartLink,
} from "@apollo/experimental-nextjs-app-support/ssr";

// Get API URL
const apiBaseUrl = process.env.NEXT_PUBLIC_API_BASE_URL;

// Create an HTTP link with the GraphQL API endpoint
const httpLink = new HttpLink({
  uri: `${apiBaseUrl}`,
});

// Create an authentication link
const authLink = setContext(async () => {

  // Return authorization headers with the token as a Bearer token
  return {
    headers: {
    },
  };
});

/**
 * Create an Apollo Client instance with the specified configuration.
 */
function makeClient() {
  return new NextSSRApolloClient({
    cache: new NextSSRInMemoryCache(),
    link:
      typeof window === "undefined"
        ? ApolloLink.from([
            new SSRMultipartLink({
              stripDefer: true,
            }),
            authLink.concat(httpLink),
          ])
        : authLink.concat(httpLink),
  });
}

/**
 * Apollo Provider
 *
 * @see https://www.apollographql.com/blog/apollo-client/next-js/how-to-use-apollo-client-with-next-js-13/
 */
export default function ApolloProvider({ children }: React.PropsWithChildren) {
  return (
    <ApolloNextAppProvider makeClient={makeClient}>
      {children}
    </ApolloNextAppProvider>
  );
}