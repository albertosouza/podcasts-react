import { HttpLink } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { registerApolloClient } from "@apollo/experimental-nextjs-app-support/rsc";
import {
  NextSSRApolloClient,
  NextSSRInMemoryCache,
} from "@apollo/experimental-nextjs-app-support/ssr";

// Get API URL
const apiBaseUrl = process.env.NEXT_SSR_API_BASE_URL;

// Create an HTTP link with the GraphQL API endpoint
const httpLink = new HttpLink({
  uri: apiBaseUrl,
  // Disable result caching
  // fetchOptions: { cache: "no-store" },
});

// Create an authentication link
const authLink = setContext(async () => {
  // Return authorization headers with the token as a Bearer token
  return {
    headers: {

    },
  };
});

/**
 * Apollo Client
 *
 * @see https://www.apollographql.com/blog/apollo-client/next-js/how-to-use-apollo-client-with-next-js-13/
 */
// eslint-disable-next-line import/prefer-default-export
export const { getClient } = registerApolloClient(
  () =>
    new NextSSRApolloClient({
      cache: new NextSSRInMemoryCache(),
      link:httpLink,
    }),
);