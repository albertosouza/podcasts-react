import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import Head from "next/head";
import 'bootstrap-icons/font/bootstrap-icons.css'
import './globals.css'
import ApolloProvider from "@/app/ApolloWrapper";

const inter = Inter({ subsets: ['latin'] })

import Footer from './components/foooter';
import Navbar from './components/navbar';

// import { loadErrorMessages, loadDevMessages } from "@apollo/client/dev";
// // if (__DEV__) {  // Adds messages only in a dev environment
//   loadDevMessages();
//   loadErrorMessages();
// // }

export const metadata: Metadata = {
  title: 'Podcast APP',
  description: 'Your awensome podcast app!',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <body className={inter.className}>
          <Navbar />
          <ApolloProvider>{children}</ApolloProvider>
          <Footer />
      </body>
    </html>
  )
}
