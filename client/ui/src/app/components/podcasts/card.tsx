"use client";

import Podcast from "@/app/types/podcast";

export default function PoscastsCard({ record }: {
  record: Podcast
}) {
  function handlePlay() {
    alert('Coming Soon!');
  }

  let img = (
    <img
      className="card-img-top"
      src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg"
    />
  );

  if (record.images?.featured) {
    img = <img className="card-img-top" src={record.images.featured} />;
  }

  return (
    <div id="cardPage" className="col mb-5">
      <div className="card h-100">
        {img}
        <div className="card-body p-4">
          <div className="text-center">
            <h5 className="fw-bolder">{record.title}</h5>
            <div>{record.categoryName}</div>
          </div>
        </div>
        <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
          <div className="text-center">
            <a className="btn btn-outline-dark mt-auto" onClick={handlePlay}>
              Play
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
