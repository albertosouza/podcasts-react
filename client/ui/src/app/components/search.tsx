"use client";
import { useSearchParams, usePathname, useRouter } from "next/navigation";

export default function Search({
  placeholder,
  loading,
}: {
  placeholder: string;
  loading: boolean;
}) {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const { replace } = useRouter();

  let typeDelay: any = null;

  function onTypeSearch(term: string) {
    if (typeDelay) {
      clearTimeout(typeDelay);
    }

    typeDelay = setTimeout(() => {
      handleSearch(term);
    }, 500);
  }

  function handleSearch(term: string) {
    const params = new URLSearchParams(searchParams);

    if (term) {
      params.set("search", term);
    } else {
      params.delete("search");
    }
    replace(`${pathname}?${params.toString()}`);
  }

  return (
    <div className="input-group mb-3">
      <span className="input-group-text">
        {loading ? (
          <i className="bi bi-arrow-clockwise"></i>
        ) : (
          <i className="bi bi-search-heart"></i>
        )}
      </span>
      <input
        id="searchPodcasts"
        className="form-control"
        placeholder={placeholder}
        onChange={(e) => {
          onTypeSearch(e.target.value);
        }}
        defaultValue={searchParams.get("search")?.toString()}
      />
    </div>
  );
}
