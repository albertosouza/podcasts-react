"use client";

import Search from "@/app/components/search";
import { gql } from "@apollo/client";
import PoscastsCard from "./components/podcasts/card";
import Podcast from "@/app/types/podcast";

import { useQuery } from "@apollo/client";

const query = gql`
  query ($imp: QueryPoscastsInput!) {
    podcasts(input: $imp) {
      id
      title
      images {
        featured
      }
      publisherName
      description
      categoryName
    }
  }
`;

interface PodcastsInput {
  search?: string;
  page?: number;
  limit?: number;
}

export default function Home({
  searchParams,
}: {
  searchParams?: {
    search?: string;
    page?: string;
  };
}) {
  const search = searchParams?.search || "";
  let podcasts: Podcast[] = [];

  let imp: PodcastsInput = {
    limit: 50,
  };
  imp.search = search;

  const { loading, error, data } = useQuery(query, {
    variables: {
      imp: imp,
    },
  });

  if (data?.podcasts) {
    podcasts = data.podcasts;
  }

  let errorMsg = <span></span>;
  if (error) {
    errorMsg = (
      <div className="alert alert-danger" role="alert">
        An error occurred while fetching data, please try again later.
      </div>
    );
  }

  return (
    <div>
      <header className="bg-dark py-5">
        <div className="container px-4 px-lg-5 my-2">
          <div className="text-center text-white">
            <h1 className="display-4 fw-bolder">Your podcasts home</h1>
            <div className="row justify-content-center mt-5">
              <div className="col-md-4">
                <Search placeholder="Search podcasts..." loading={loading} />
              </div>
            </div>
          </div>
        </div>
      </header>
      <section className="py-5">
        {errorMsg}
        <div className="container px-4 px-lg-5 mt-5 h-50 ">
          {loading ? <div className="text-center mb-5">Loading...</div> : null}
          <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            {!podcasts || (podcasts.length == 0 && !loading) ? (
              <div className="text-center">No podcasts found.</div>
            ) : null}
            {podcasts
              ? podcasts.map((podcast) => (
                  <PoscastsCard
                    key={podcast.id}
                    record={podcast}
                  ></PoscastsCard>
                ))
              : null}
          </div>
        </div>
      </section>
    </div>
  );
}
