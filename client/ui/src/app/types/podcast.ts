export default interface Podcast {
  id: string;
  title: string;
  images: Image;
  description: string;
  publisherName: string;
  publisherId: string;
  categoryId: string;
  categoryName: string;
  hasFreeEpisodes: boolean;
  playSequence: string;
  isExclusive: boolean;
  mediaType: string;
}

export interface Image {
  default: string;
  featured: string;
  thumbnail: string;
  wide: string;
}