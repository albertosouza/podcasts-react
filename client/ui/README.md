This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

See README.md in root of this repo

## Notes:

How to build a new image and push to repo:

```
docker build --tag ccp-ui:release --target release .
docker tag ccp-ui:release $REPO_IMAGE:$IMAGE_NAME
docker push $REPO_IMAGE:$IMAGE_NAME

```

Test the container locally:

```
docker run -it ccp-ui:release ./main -h
docker run -it ccp-ui:release ./main start-server
```
